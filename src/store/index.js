import Vue from "vue";
import Vuex from "vuex";
import timelineService from "@/services/timelineService";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        posts: [],
    },
    mutations: {
        SET_POSTS(state, posts) {
            state.posts = posts;
        },
        ADD_POST(state, post) {
            state.posts.unshift(post);
        },
    },
    actions: {
        async fetchInitialPosts({commit}) {
            const posts = await timelineService.getInitialPosts();
            commit("SET_POSTS", posts);
        },
        async addNewPost({commit}, text) {
            const newPost = await timelineService.addPost(text);
            commit("ADD_POST", newPost);
        },
    },
    getters: {
        getPosts: (state) => state.posts,
    },
});
