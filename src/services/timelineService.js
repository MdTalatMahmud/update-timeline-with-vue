const initialPosts = [];

// Simulate an asynchronous service call with a delay
const simulateServiceCall = async (data) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(data);
        }, 500); // Simulate a 500ms delay
    });
};

// Service layer
const timelineService = {
    // Get initial posts
    getInitialPosts: async () => {
        return simulateServiceCall(initialPosts);
    },

    // Add a new post
    addPost: async (text) => {
        const newPost = {
            id: initialPosts.length + 1,
            text,
            timestamp: new Date(),
        };

        // Simulate updating the database with a delay
        await simulateServiceCall();

        // Update the local data
        initialPosts.unshift(newPost);

        return simulateServiceCall(newPost);
    },
};

export default timelineService;
